#include<iostream>
#include<string>
#include<string.h>
#include<map>
#include<vector>
#include<fstream>
#include<cstdlib>
#include<stdio.h>
using namespace std;

#define LIMIT 100

int main() {
	ifstream inf;
	inf.open("list_products_out.txt");
	
	string company_name;
	string company_link, line;

	string system_cmd;
	char cmd[1024];

	int cnt = 0;
	string name_temp;
	while(inf.good()) {
		getline(inf, company_name);
		getline(inf, company_link);
		getline(inf, line);
		system_cmd = "";

		/* Remove quotes from name and link */
		if(company_name == "" || company_link == "") break;
		company_name = company_name.substr(1, 
						company_name.size() - 2);
		company_link = company_link.substr(1,
                                                company_link.size() - 2);

		name_temp = "";
		name_temp = company_name;
		for(int index = 0; index < (int)name_temp.size(); ++index) {
			if(name_temp[index] == ' ')
				name_temp[index] = '+';
		}

		//cout<<"name_temp = "<<name_temp<<endl;
			

		/* Conctruct the wget command to download the company data */
		system_cmd += "wget \"http://api.crunchbase.com/v/1/products/posts?name=";
		system_cmd += name_temp;
		system_cmd += "&api_key=wg4p8s4qptdzsrjcwagh2j6b\" -O ";
		system_cmd += company_link;
		system_cmd += "_posts_prods.json";

		//cout<<"system_cmd = "<<system_cmd<<endl;

		strcpy(cmd, system_cmd.c_str());
		system(cmd);

		/* Handle if there is some constraint on data collection rate */
		//sleep(1);
		//if(cnt > LIMIT) break;
		//++cnt;
	}

	inf.close();
	return (0);
}
