#include<iostream>
#include<string>
#include<string.h>
#include<map>
#include<vector>
#include<fstream>
#include<cstdlib>
#include<stdio.h>
using namespace std;

#define LIMIT 500

int main() {
	ifstream inf;
	inf.open("list_people_out.txt");
	
	string first_name;
	string last_name;
	string person_link, line;

	string system_cmd;
	char cmd[1024];

	int cnt = 0;
	while(inf.good()) {
		getline(inf, first_name);
		getline(inf, last_name);
		getline(inf, person_link);
		getline(inf, line);
		system_cmd = "";

		/* Remove quotes from name and link */
		if(person_link == "") break;
		first_name = first_name.substr(1, 
						first_name.size() - 2);
		last_name = last_name.substr(1, 
						last_name.size() - 2);
		person_link = person_link.substr(1,
                                                person_link.size() - 2);


		for(int index = 0; index < (int)first_name.size(); ++index) {
			if(first_name[index] == ' ') 
				first_name[index] = '+';
		}

                for(int index = 0; index < (int)last_name.size(); ++index) {
                        if(last_name[index] == ' ')
                                last_name[index] = '+';
                }

		/* Conctruct the wget command to download the company data */
		system_cmd += "wget \"http://api.crunchbase.com/v/1/people/posts?first_name=";
		system_cmd += first_name;
		system_cmd += "&last_name=";
		system_cmd += last_name;
		system_cmd += "&api_key=wg4p8s4qptdzsrjcwagh2j6b\" -O ";
		system_cmd += person_link;
		system_cmd += "_posts_people.json";
		
		strcpy(cmd, system_cmd.c_str());
		system(cmd);

		/* Handle if there is some constraint on data collection rate */
		//sleep(1);
		//if(cnt > LIMIT) break;
		//++cnt;
	}

	inf.close();
	return (0);
}
