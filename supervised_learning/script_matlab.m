tic
fid = fopen('WO.txt');
C = {};
while ~feof(fid)  % repeat until end of file is reached
  c = fscanf(fid, '%s', 1);  % read a string
  C = [C, c];
end

WO = C';

load('DS.txt')
load('WS.txt')

T = 5;
BETA = 0.01;
ALPHA = 50/T;
N = 100;
SEED = 3;
OUTPUT = 1;
[WP, DP, Z] = GibbsSamplerLDA(WS, DS, T, N, ALPHA, BETA, SEED, OUTPUT);
toc
[S]= WriteTopics(WP, BETA, WO, 20, 10);
S(1:5)


